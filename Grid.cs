﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GridNya : MonoBehaviour
{
    public GameObject monster;
    public Vector3 gridWorldSize;
    public LayerMask unWalkable;
    public float nodeRadius;
    float nodeDiameter;
    int gridX, gridY;
    Node[,] node;
    

    void Start()
    {
        nodeDiameter = nodeRadius * 2;
        gridX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridY = Mathf.RoundToInt(gridWorldSize.z / nodeDiameter);
        CreateNode();
    }
    void CreateNode()
    {
        node = new Node[gridX, gridY];
        Vector3 pointBtnLeft= transform.position-Vector3.right*gridWorldSize.x/2-Vector3.forward* gridWorldSize.z / 2;
        for (int x = 0;x< gridX;x++)
        { 
           for (int y = 0;y< gridY;y++)
          {
                Vector3 pointCenter = pointBtnLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
                bool walkable = !Physics.CheckSphere(pointCenter, nodeRadius);
                node[x, y] = new Node(walkable, pointCenter, x, y);
          }

        }
    }

    public List<Node> GetNeighbor(Vector3 currentpoint)
    {
        List<Node> neighborNodes = new List<Node>();
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                    continue;

                var neighborNodeX = nodeCenterPoint.nodeX + x;
                var neighborNodeY = nodeCenterPoint.nodeY + y;
                if (neighborNodeX >= 0 && neighborNodeX < gridX && neighborNodeY >= 0 && neighborNodeY < gridY)
                {
                    neighborNodes.Add(node[neighborNodeX, neighborNodeY]);
                }
            }
        }
        return neighborNodes;
    }




    public Node TargetNodeOnWorld(Vector3 targetPos)
    {
        float percentX=(targetPos.x + gridWorldSize.x / 2) / gridWorldSize.x;
        float percentY = (targetPos.z + gridWorldSize.z / 2) / gridWorldSize.z;
        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);

        int x= Mathf.RoundToInt((gridX - 1) * percentX);
        int y = Mathf.RoundToInt((gridY - 1) * percentY);
        return node[x, y];

    }

    public List<Node> path;
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, gridWorldSize);

        if(node != null)
        {
            Node targetNode = TargetNodeOnWorld(monster.transform.position);
        foreach (Node n in node) 
        {
                if (targetNode == n)
                {
                    Gizmos.color = Color.cyan;
                }
                else if (n.walkable)
                {
                    Gizmos.color = Color.yellow;
                }
                else
                {
                    Gizmos.color = Color.red;
                }
                if (path != null)
                {
                    if (path.Contains(n))
                    {
                        Gizmos.color = Color.magenta;
                    }
                }
                Gizmos.DrawCube(n.worldpos, Vector3.one * (nodeDiameter - 0.1f));
        }
        }
    }
}
